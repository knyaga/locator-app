
/**
 * Created by otis on 2016-08-10.
 */

(function($, window, document,google){

    $(function(){

        /** ============================================================
         * Implement Place class
         * @constructor @Getters @Setters
         * ==============================================================*/

        var Place = function(){};

        Place.prototype.setCountry = function(country) {
            this.country = country;
        }

        Place.prototype.setAddress =function(address) {
            if(address.length != 0) {
                address = address.split(',');
                this.address = address[0];
            }
        }

        Place.prototype.setCity = function(city) {
            this.city = city;
        }

        Place.prototype.setLat = function(lat) {
            this.lat = lat;
        }

        Place.prototype.setLng = function(lng) {
            this.lng = lng;
        }

        Place.prototype.getAddress = function() {
            return this.address;
        }

        Place.prototype.getCity = function() {
            return this.city;
        }

        Place.prototype.getCountry = function() {
            return this.country;
        }


        /**========= VARIABLES ===============***/

        var $searchForm = $("#searchForm");
        var $inputField = $searchForm.find('input');
        var placesArray = [];

        /**========= UTILS SECTION ===============***/
        $("#mainContainer").css("height",$(window).height());
        $('#resultsTable').tablesorter();


        /**========= EVENTS SECTION ===============***/

        $searchForm.on('submit', function(e){
            e.preventDefault();

            geocodeAddress($inputField.val());

            $('#results').css('display','');
        });

        //When table row is clicked
        var selectedPlace;

        $('table').on('click', 'a#viewMap', function(event) {
            var rowindex = $(this).closest('tr').index() - 1;

            //grab the object and save to local storage
            selectedPlace = placesArray[rowindex];

            saveSelectedPlace();

        });


        /** Disable Locate button on empty address field */

        $inputField.on('keyup', function(){
            var isEmpty    = false,
                $locateBtn = $searchForm.find('button');

            $searchForm.find('input').each(function(){
                if( $(this).val().length == 0 ) {
                    isEmpty = true
                }
            });

            if (isEmpty) {
                $locateBtn.attr('disabled','disabled');

            }else{
                $locateBtn.removeAttr('disabled','disabled');
            }
        });

        /**========= FUNCTIONS ================================================
         * Function declarations section
         * =================================================================***/

        /**
         * Returns a address details
         * @returns {*}
         */

        function geocodeAddress(providedAddress) {

            var geocoder = new google.maps.Geocoder();

                var request = {
                    address: providedAddress
                };

                geocoder.geocode(request, function(results, status){

                //Assumption: all status error will display no results
                if (status != google.maps.GeocoderStatus.OK) {
                    sweetAlert('', 'No results were found.','error');
                    return;
                }

                if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                    sweetAlert('', 'No results were found.','error');
                    return;
                }

                if (status == google.maps.GeocoderStatus.INVALID_REQUEST) {
                    sweetAlert('', 'No results were found.','error');
                    return;
                }

                processData(results);

                updateResultsTable();
            });

        }

        /**
         * Process data
         * @param results
         */
        function processData(results) {

            var place = {};

            if(results.length > 0 ) {
                for (var i=0; i < results.length; i++) {

                    var addressParts = results[i].formatted_address;

                    if(addressParts.length != 0) {
                        addressParts = addressParts.split(',');

                        var address = addressParts[0],
                            city    = addressParts[1],
                            country = addressParts[addressParts.length - 1];

                        var lat = results[i].geometry.location.lat(),
                            lng = results[i].geometry.location.lng();

                        place = new Place();
                        place.setAddress(address);
                        place.setCity(city);
                        place.setCountry(country);
                        place.setLat(lat);
                        place.setLng(lng);

                        //reset array before push
                        placesArray = [];
                        placesArray.push(place);
                    }
                }
            }

        }

        /**
         * Saves the selected Place in localStorage as key: 'place' : default
         */
        function saveSelectedPlace()
        {
            var placeObj = JSON.stringify(selectedPlace);
            localStorage.setItem('place',placeObj);
        }

        /**
         * Populates table with data
         */
        function updateResultsTable() {

            if(placesArray.length > 0) {
                for(var i=0; i < placesArray.length; i++) {
                    $("#resultsSection").after("<tr><td>"
                        + placesArray[i].getAddress() + "</td><td>"
                        + placesArray[i].getCity() + "</td><td>"
                        + placesArray[i].getCountry() + "</td><td>" +
                        "<a id='viewMap' target='_blank' href='/map.html'>View</a></td></tr>"
                    );
                }
            }
        }

        /**
         * Refreshes the tables and remove the previous store
         */
        function reload()
        {
            var place = localStorage.getItem('place');
            if (place) {
                localStorage.removeItem('place')
            }
            updateResultsTable();
        }

        /**
         * ================== END OF FUNCTIONS ==================================
         */

    }); //End of $()

})($, window, document, google);
