/**
 * Created by otis on 2016-08-14.
 */
(function($, google, window){

    //Makes height of map == window height
    $("#map").css("height",$(window).height());

    function initMap() {

        //Checks if key: place exists
        var key = localStorage.getItem('place'),
            place = {};
        if(typeof key != null ) {
            place = JSON.parse(key);
        }

        //Check if place object is empty
        if(!isEmpty(place)) {

            var location  = new google.maps.LatLng(place.lat, place.lng);
            var mapCanvas = document.getElementById('map');

            var config = {
                center: location,
                zoom: 12,
                panControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };



            var map = new google.maps.Map(mapCanvas, config);
            //Style from  Snazzy Maps
            var styles =  [
                {
                    featureType: 'all',
                    stylers: [
                        { saturation: -80 }
                    ]
                },{
                    featureType: 'road.arterial',
                    elementType: 'geometry',
                    stylers: [
                        { hue: '#00ffee' },
                        { saturation: 50 }
                    ]
                },{
                    featureType: 'poi.business',
                    elementType: 'labels',
                    stylers: [
                        { visibility: 'off' }
                    ]
                }
            ];

            map.set('styles',styles);

            var markerImage = '/images/marker.png';

            var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: markerImage
            });

            var contentString = '<div class="info-window">' +
                '<h3>Address details</h3>' +
                '<div class="info-content">' +
                '<p>Address:' + place.address + '</p>' + '<br>' +
                '<p>City:' + place.city    + '</p>' + '<br>' +
                '<p>Country:' + place.country  + '</p>' + '<br>' +
                '</div></div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 400
            });
            infowindow.open(map, marker);

            marker.addListener('click', function(){
                infowindow.open(map, marker);
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initMap);

    /**
     * Checks if object is empty
     * return Boolean
     */
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    function isEmpty(obj) {

        // null and undefined are "empty"
        if (obj == null) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        // If it isn't an object at this point
        // it is empty, but it can't be anything *but* empty
        // Is it empty?  Depends on your application.
        if (typeof obj !== "object") return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }

})($, google, window);